#!/bin/sh

g++ *.cpp -o hatify.out

#octave octave_scripts/edge_detect.m

./hatify.out

convert colored_edges.ppm colored_edges.png

rm colored_edges.ppm
rm hatify.out
#rm magnitude.txt
#rm orientation.txt
