#include "edge_node.h"
#include "image.h"

#include <fstream>
#include <iostream>
#include <cassert>
#include <math.h>

Color Rainbow(double curvature, double max_curvature);

double*** load_files(const std::string& mag_fname,
                    const std::string& orient_fname,
										int& width, int& height) {
	std::ifstream mag_f(mag_fname), orient_f(orient_fname);
	if (!mag_f.good()) {
		std::cerr << "ERROR: Invalid magnitude file " << mag_fname << std::endl;
		return nullptr;
	}
	if (!orient_f.good()) {
		std::cerr << "ERROR: Invalid orientation file " << orient_fname << std::endl;
		return nullptr;
	}

	mag_f >> width >> height;

	double ***data = new double**[3]; // Will return all image data

	// Store image properties
	data[0] = new double*[1];
	data[0][0] = new double[2];
	data[0][0][0] = (double) width;
	data[0][0][1] = (double) height;

	data[1] = new double*[height];
	data[2] = new double*[height];

	// Copy the data into the array
	double tmp;
	for (int y=0; y<height; y++) {
		data[1][y] = new double[width];
		data[2][y] = new double[width];

		for (int x=0; x<width; x++) {
			mag_f >> tmp;
			data[1][y][x] = tmp;
			
			orient_f >> tmp;
			data[2][y][x] = tmp;
		}
	}
	
	return data;
}

int main(int argc, char **argv) {

	std::string mag_filename, orient_filename;
	if (argc > 1) mag_filename = argv[1];
	else mag_filename = "magnitude.txt";
	if (argc > 2) orient_filename = argv[2];
	else orient_filename = "orientation.txt";

	int width, height;

	double ***data = load_files(mag_filename, orient_filename, width, height);
	assert(data != nullptr);

	bp_t& edge_nodes = generate_edges(data);
	std::cout << edge_nodes.size() << " total edge nodes" << std::endl;

	double max_curvature = -DBL_MAX;
	for (std::pair<std::pair<int,int>,EdgeNode*> e : edge_nodes) {
		if (e.second->curvature > max_curvature) max_curvature = e.second->curvature;
	}
	std::cout << "Maximum curvature: " << max_curvature << std::endl;

	Image<Color> output;
	output.Allocate(width, height);
	for (int x=0; x<width; x++) {
		for (int y=0; y<height; y++) {
			double curvature = 0;

			bp_t::iterator itr = edge_nodes.find(std::make_pair(x,y));
			if (itr != edge_nodes.end()) {
				curvature = itr->second->curvature;
			}


			output.SetPixel(x,y,Rainbow(curvature, max_curvature));
		}
	}

	output.Save("colored_edges.ppm");

	for (std::pair<std::pair<int,int>,EdgeNode*> e : edge_nodes) delete e.second;
	delete &edge_nodes;

	return 0;
}

Color Rainbow(double curvature, double max_curvature) {
  Color answer;
  if (curvature < EPS) {
    // black
    answer.r = 0; answer.g = 0; answer.b = 0;
  } else if (curvature < 0.2*max_curvature) {
    // blue -> cyan
    double tmp = curvature * 5.0 / max_curvature;
    answer.r = 0;
    answer.g = tmp*255;
    answer.b = 255;
  } else if (curvature < 0.4*max_curvature) {
    // cyan -> green
    double tmp = (curvature-0.2*max_curvature) * 5.0 / max_curvature;
    answer.r = 0;
    answer.g = 255;
    answer.b = (1-tmp*tmp)*255;
  } else if (curvature < 0.6*max_curvature) {
    // green -> yellow
    double tmp = (curvature-0.4*max_curvature) * 5.0 / max_curvature;
    answer.r = sqrt(tmp)*255;
    answer.g = 255;
    answer.b = 0;
  } else if (curvature < 0.8*max_curvature) {
    // yellow -> red
    double tmp = (curvature-0.6*max_curvature) * 5.0 / max_curvature;
    answer.r = 255;
    answer.g = (1-tmp*tmp)*255;
    answer.b = 0;
  } else if (curvature < max_curvature) {
    // red -> white
    double tmp = (curvature-0.8*max_curvature) * 5.0 / max_curvature;
    answer.r = 255;
    answer.g = tmp*255;
    answer.b = tmp*255;
  } else {
    // white
    answer.r = answer.g = answer.b = 255;
  }
  return answer;
}
