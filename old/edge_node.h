// File: edge_node.h

#ifndef EDGENODE_H
#define EDGENODE_H

#define EPS 0.001
#define CURVATURE_CHECK 1 // Number of nodes on each side of a node to use for curvature estimation

#define WEAK_MAX 0.2
#define WEAK_MIN 0.01

#include <float.h>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <iostream>

class PairHash {
public:
	int operator()(const std::pair<int,int>& p) const {
		return p.first + 10000*p.second;
	}
};

struct EdgeNode;
typedef std::unordered_map<std::pair<int,int>, EdgeNode*, PairHash> bp_t;

struct EdgeNode {
	typedef std::list<EdgeNode*> neighbor_t;

	EdgeNode(std::pair<int,int> pos, const double& mag, const double& grad)
		: x(pos.first), y(pos.second), magnitude(mag), gradient(grad), curvature(-DBL_MAX) {}

	void connect(EdgeNode* new_node);
	void generate_curvature();

	int x,y;
	double magnitude, gradient, curvature;

	neighbor_t neighbors;
};

bp_t& generate_edges(double*** data);
#endif
