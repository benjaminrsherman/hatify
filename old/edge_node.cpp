// File: edge_node.cpp

#include <math.h>

#include "edge_node.h"
#include <cassert>
#include <algorithm>

// Returns the difference in gradient between the first and second edges after normalizing
// each gradient to [0, M_PI], or DBL_MAX if either pointer is null
double gradient_diff(const EdgeNode* first, const EdgeNode* second) {
	if (first==nullptr || second==nullptr) return DBL_MAX;
	double g1 = first->gradient;
	double g2 = second->gradient;

	// Ensure gradients are within a normal bound. Using abs() removes floating-point issues
	g1 = abs(fmod(g1, 2*M_PI));
	g2 = abs(fmod(g2, 2*M_PI));

	return abs(g1-g2);
}

// Adds a pointer to @new_node in @this->neighbors while keeping @this->neighbors ordered
void EdgeNode::connect(EdgeNode* new_node) {
	neighbor_t::iterator itr = this->neighbors.begin();
	while (itr != neighbors.end()) {
		if (gradient_diff(this,new_node)<gradient_diff(this, *itr)) {
			// This is (probably) along the same edge, so put it earlier in the list
			this->neighbors.insert(itr, new_node);
			return;
		}
		itr++;
	}

	// If we got here, the new node should be inserted at the end
	this->neighbors.push_back(new_node);
}

// Recursive helper function for EdgeNode::generate_curvature()
double calculate_curvature(const EdgeNode* node, const EdgeNode* prev, int& remaining) {
	if (remaining > CURVATURE_CHECK) return 0;
	
	double local_curvature = gradient_diff(node, prev);

	for (const EdgeNode* neighbor : node->neighbors)
		if (neighbor != prev) // Make sure we don't accidentally check a node twice
			return local_curvature + calculate_curvature(neighbor, node, ++remaining);

	return 0;
}

// Estimates the curvature of a node by averaging the local curvatures of CURVATURE_CHECK
// surrounding nodes, or every surrounding node.  Specifically, this will use up to
// CURVATURE_CHECK/2 nodes on either side of this node for averaging, only stopping if
// the edge ends.
void EdgeNode::generate_curvature() {
	if (this->neighbors.size() == 0) return;

	neighbor_t::iterator itr = this->neighbors.begin();

	int num_first_checked=1, num_second_checked=0;
	curvature = calculate_curvature(*itr, this, num_first_checked);

	if (this->neighbors.size() > 1) {
		itr++;

		num_second_checked = 1;
		curvature += calculate_curvature(*itr, this, num_second_checked);
	}

	curvature /= num_first_checked + num_second_checked; // Average the value
}

// Inserts a new node into the graph and connects it to any neighbors
// Returns true if the node already existed
bool insert(EdgeNode* new_node, bp_t& graph) {
	// Ensure that this is a new node
	bp_t::iterator itr = graph.find(std::make_pair(new_node->x, new_node->y));
	if (itr != graph.end()) return true;

	graph.insert(std::make_pair(std::make_pair(new_node->x, new_node->y), new_node));

	// Connect to all neighboring nodes
	for (int xoff=-1; xoff<=1; xoff++) {
		for (int yoff=-1; yoff<=1; yoff++) {
			if (xoff==0 && yoff==0) continue;
			
			itr = graph.find(std::make_pair(new_node->x + xoff, new_node->y + yoff));
			if (itr != graph.end()) {
				// All connections are bidirectional
				itr->second->connect(new_node);
				new_node->connect(itr->second);
			}
		}
	}

	return false;	
}

// Recursively adds all nodes connected to a strong node into @graph
// Only edges which are stronger than WEAK_MIN are added
void track_edge(const EdgeNode* node, bp_t* graph) {
	for (EdgeNode* neighbor : node->neighbors) {
		if (neighbor->magnitude > WEAK_MIN) { // Ensure that we're adding a valid edge
			std::pair<int,int> loc(neighbor->x, neighbor->y);

			std::pair<bp_t::iterator,bool> p = graph->insert(std::make_pair(loc, neighbor));
			if (p.second) track_edge(neighbor, graph); // Only continue if the neighbor was inserted
		}
	}
}

// Removes all connections to @node and removes @node from @graph
void remove_node(EdgeNode* node, bp_t& graph) {
	for (EdgeNode* neighbor : node->neighbors) {
		assert(std::count(node->neighbors.begin(),node->neighbors.end(),neighbor)==1); // Make sure this neighbor is only in here once
		assert(std::count(neighbor->neighbors.begin(),neighbor->neighbors.end(),node)); // Make sure we're a neighbor of the neighbor
		neighbor->neighbors.remove(node);
	}

	// If the node is in the graph, remove it
	graph.erase(std::make_pair(node->x, node->y));

	delete node;
}

// A node is at the end of a line if there are 5 contiguous neighbor
// slots which are empty (ie there are up to 3 connected neighbors)
// TODO: This is probably broken
bool end_of_line(const EdgeNode* node) {
	if (node->neighbors.size() > 3) return false;
	
	unsigned int num_neighbors[4] = {0,0,0,0}; // Each index represents a side of the node

	for (const EdgeNode* neighbor : node->neighbors) {
		int xoff = neighbor->x - node->x;
		int yoff = neighbor->y - node->y;

		if (xoff != 0) num_neighbors[xoff+1]++;
		if (yoff != 0) num_neighbors[yoff+2]++;
	}

	for (int i=0; i<4; i++) {
		if (num_neighbors[i] == node->neighbors.size()) return true;
	}

	return false;
}

// Checks if @n1 is connected to @n2 by circling @ignore
// NOTE: @n1 must be to the upper right of @n2 (lower or equal x- and y-coordinates)
bool check_connectivity(const EdgeNode* n1, const EdgeNode* n2, const EdgeNode* ignore) {
	int xoff = n1->x - n2->x;
	int yoff = n1->y - n2->y;
	int sq_distance = xoff*xoff + yoff*yoff;

	for (const EdgeNode* n : n1->neighbors) {
		if (n == n2) return true;
		if (n == ignore) continue;

		int local_xoff = n->x - n2->x;
		int local_yoff = n->y - n2->y;
		int local_sq_distance = local_xoff*local_xoff + local_yoff*local_yoff;

		if (local_sq_distance < sq_distance) {
			if (check_connectivity(n, n2, ignore)) return true;
		}
	}

	return false;
}

// Returns true if removal of this 
bool is_bridge(const EdgeNode* node, const bp_t& graph) {
	for (const EdgeNode* n1 : node->neighbors) {
		for (const EdgeNode* n2 : node->neighbors) {
			if (n1 == n2) continue;

			bool connected = check_connectivity(n1, n2, node);

			if (!connected) return true;
		}
	}
	return false;
}

// Performs one round of edge thinning and returns the number of removed edges
// https://en.wikipedia.org/wiki/Edge_detection#Edge_thinning
int thin_edges(bp_t& graph) {
	int removed = 0;

	for (int xoff=-1; xoff<=1; xoff++) {
		for (int yoff=-1; yoff<=1; yoff++) {
			if ((xoff!=0 && yoff!=0) || (xoff==0 && yoff==0)) continue;

			bp_t::iterator itr = graph.begin();
			while (itr != graph.end()) {
				// Check if there is a neighbor in the given direction
				std::pair<int,int> test_loc(itr->second->x + xoff, itr->second->y + yoff);
				if (graph.find(test_loc) == graph.end()) {
					itr++;
					continue;
				}

				if (end_of_line(itr->second)) {
					itr++;
					continue;
				}

				if (is_bridge(itr->second, graph)) {
					itr++;
					continue;
				}

				// We can only get here if the node is supposed to be removed
				EdgeNode *to_remove = itr->second;
				itr++;

				remove_node(to_remove, graph);
				std::cout << removed << std::endl;
				removed++;
			}
		}
	}
	
	return removed;
}

bp_t& generate_edges(double*** data) {
	const int width = (int)data[0][0][0], height = (int)data[0][0][1];

	bp_t all_nodes;

	for (int y=0; y<height; y++) {
		for (int x=0; x<width; x++) {
			double& mag = data[1][y][x];
			if (abs(mag) > EPS) {
				double& grad = data[2][y][x];
				EdgeNode *new_node = new EdgeNode(std::make_pair(x,y), mag, grad);

				// If this returns true, the node was not inserted
				assert(!insert(new_node, all_nodes));
			}
		}
	}

	// THERE ARE NO ERRORS BEFORE HERE

	// Perform edge tracking via hysteresis before calculating curvature
	bp_t *strong_nodes = new bp_t();
	for (std::pair<std::pair<int,int>, EdgeNode*> node_pair : all_nodes) {
		if (node_pair.second->magnitude > WEAK_MAX) {
			std::pair<bp_t::iterator,bool> p = strong_nodes->insert(node_pair);
			if (p.second) track_edge(node_pair.second, strong_nodes);
		}
	}

	for (std::pair<std::pair<int,int>, EdgeNode*> node_pair : all_nodes) {
		if (strong_nodes->find(node_pair.first) == strong_nodes->end()) {
			// Remove all connections to this node
			remove_node(node_pair.second, all_nodes);
		}
	}

	std::cout << "Beginning to thin edges" << std::endl;

	int thinned = thin_edges(*strong_nodes);
	while (thinned > 0) thinned = thin_edges(all_nodes);

	for (std::pair<std::pair<int,int>, EdgeNode*> node_pair : *strong_nodes)
		node_pair.second->generate_curvature();


	return *strong_nodes;
}
