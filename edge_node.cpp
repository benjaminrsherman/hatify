// File: edge_node.cpp

#include "edge_node.h"
#include <iostream>
#include <list>

// Connects two nodes in the graph
void connect(EdgeNode* first, EdgeNode* second) {
	int xoff = first->x - second->x + 1;
	int yoff = first->y - second->y + 1;
	if (xoff==0) first->neighbors[7-yoff] = second;
	else if (xoff==2) first->neighbors[yoff+1] = second;
	else first->neighbors[xoff<<2] = second;

	xoff--, yoff--;
	xoff*=-1;
	yoff*=-1;
	xoff++, yoff++;
	if (xoff==0) second->neighbors[7-yoff] = first;
	else if (xoff==2) second->neighbors[yoff+1] = first;
	else second->neighbors[xoff<<2] = first;

	first->num_neighbors++;
	second->num_neighbors++;
}

// Disconnects a node from the graph. This assumes that the graph was properly constructed
void disconnect_node(EdgeNode* node) {
	for (int i=0; i<8; i++) {
		if (!node->neighbors[i]) continue;
		node->neighbors[i]->neighbors[(i+4)%8] = NULL;
		node->neighbors[i]->num_neighbors--;
	}
}

// Inserts @node into @graph and connects it to all neighbors
// NOTE: this assumes that @graph does not already contain @node
void insert(Graph& graph, EdgeNode* node) {
	for (int xoff=-1; xoff<=1; xoff++) {
		for (int yoff=-1; yoff<=1; yoff++) {
			Graph::iterator neighbor_itr = graph.find(std::make_pair(node->x + xoff, node->y + yoff));
			if (neighbor_itr == graph.end()) continue;

			connect(node, neighbor_itr->second);
		}
	}	

	graph.insert(std::make_pair(std::make_pair(node->x, node->y), node));
}

// Recursively adds all nodes connected to a strong node into @graph
// Only edges which are stronger than WEAK_MIN are added
void track_edge(EdgeNode* node, Graph* graph) {
	if (!node) return;

	if (node->magnitude > WEAK_MAX) {
		std::pair<int,int> loc(node->x, node->y);
		
		std::pair<Graph::iterator,bool> p = graph->insert(std::make_pair(loc, node));
		if (p.second)
			for (EdgeNode* neighbor : node->neighbors) track_edge(neighbor, graph);
	} else if (node->magnitude > WEAK_MIN) {
		for (EdgeNode* neighbor : node->neighbors) {
			if (!neighbor) continue;
			std::pair<int,int> n_loc(neighbor->x, neighbor->y);
			if (graph->count(n_loc)) {
				std::pair<int,int> loc(node->x, node->y);

				std::pair<Graph::iterator,bool> p = graph->insert(std::make_pair(loc,node));
				if (p.second)
					for (EdgeNode* neighbor : node->neighbors) track_edge(neighbor, graph);

				return;
			}
		}
	}
}

int count_transitions(const EdgeNode* node) {
	int ret=0;
	for (int i=0; i<8; i++) {
		if ((node->neighbors[i]==NULL)^(node->neighbors[(i+1)%8]==NULL)) ret++;
	}
	return ret;
}

// Thins edges in @graph
// Algorithm modified from http://fourier.eng.hmc.edu/e161/lectures/morphology/node2.html
void thin_edges(Graph* graph) {
	int thinned;
	std::list<std::pair<int,int>> to_erase;
	do {
		thinned = 0;
		for (std::pair<std::pair<int,int>,EdgeNode*> edge_pair : *graph) {
			if (edge_pair.second->num_neighbors<2 || edge_pair.second->num_neighbors>6) continue;
			if (count_transitions(edge_pair.second)>2) continue;
			if (edge_pair.second->neighbors[0] && edge_pair.second->neighbors[2] && edge_pair.second->neighbors[4]) continue;
			if (edge_pair.second->neighbors[2] && edge_pair.second->neighbors[4] && edge_pair.second->neighbors[6]) continue;
			disconnect_node(edge_pair.second);
			to_erase.push_back(edge_pair.first);
			thinned++;
		}
		for (std::pair<int,int> coordinates : to_erase) {
			delete graph->at(coordinates);
			graph->erase(coordinates);
		}
		to_erase.clear();
		for (std::pair<std::pair<int,int>,EdgeNode*> edge_pair : *graph) {
			if (edge_pair.second->num_neighbors<2 || edge_pair.second->num_neighbors>6) continue;
			if (count_transitions(edge_pair.second)>2) continue;
			if (edge_pair.second->neighbors[0] && edge_pair.second->neighbors[2] && edge_pair.second->neighbors[6]) continue;
			if (edge_pair.second->neighbors[0] && edge_pair.second->neighbors[4] && edge_pair.second->neighbors[6]) continue;
			disconnect_node(edge_pair.second);
			to_erase.push_back(edge_pair.first);
			thinned++;
		}
		for (std::pair<int,int> coordinates : to_erase) {
			delete graph->at(coordinates);
			graph->erase(coordinates);
		}
		to_erase.clear();
		std::cout << thinned << " edges thinned.\n";
	} while (thinned > 0);
}

Graph& generate_edges(double*** data) {
	double width = data[0][0][0], height = data[0][0][1];

	Graph raw_edges;
	for (int y=0; y<height; y++) {
		for (int x=0; x<width; x++) {
			if (data[1][y][x] < EPS) continue;

			EdgeNode *node = new EdgeNode(x, y, data[1][y][x], data[2][y][x]);
			insert(raw_edges, node);
		}
	}

	std::cout << raw_edges.size() << std::endl;

	Graph *true_edges = new Graph();

	// Add true edges to @true_edges
	for (std::pair<std::pair<int,int>,EdgeNode*> edge_pair : raw_edges) {
		track_edge(edge_pair.second, true_edges);
	}

	// Remove connections to invalid edges
	for (std::pair<std::pair<int,int>,EdgeNode*> edge_pair : raw_edges) {
		if (!true_edges->count(edge_pair.first)) {
			disconnect_node(edge_pair.second);
			delete edge_pair.second;
		}
	}

	thin_edges(true_edges);

	std::cout << true_edges->size() << std::endl;

	return *true_edges;
}
