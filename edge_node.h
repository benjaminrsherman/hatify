// File: edge_node.h

#include <unordered_map>
#include <cstring>

#define EPS 0.001
#define CURVATURE_CHECK 1 // Number of nodes on each side of a node to use for curvature estimation

#define WEAK_MAX 0.7
#define WEAK_MIN 0.15

class CoordinateFunctor {
public:
	// Fast hash operation that is probably collision free (unless a dimension of the
	// image is greater than 2^16)
	uint32_t operator()(const std::pair<uint32_t,uint32_t>& coord) const {
		return coord.first | (coord.second << 16);
	}
};

struct EdgeNode;
typedef std::unordered_map<std::pair<uint32_t,uint32_t>, EdgeNode*, CoordinateFunctor> Graph;

struct EdgeNode
{
	EdgeNode(int xpos, int ypos, double mag, double grad) :
		x(xpos), y(ypos), magnitude(mag), gradient(grad), num_neighbors(0)
	{memset(neighbors,0,8*sizeof(EdgeNode*));}

	EdgeNode* neighbors[8];

	int x, y, num_neighbors;
	double magnitude, gradient, curvature;
};

Graph& generate_edges(double*** data);
